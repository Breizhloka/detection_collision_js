let canvas = document.querySelector('#canvas');
let ctx = canvas.getContext('2d');

let canvasW = 1024;
let canvasH = 768;
canvas.width = canvasW;
canvas.height = canvasH;
let radius = 60;
let xMouse, yMouse;
let collisionFlag = false;

// Mes cercles
let shapes = [
  //{x:canvasW/2, y:canvasH/2, radius:radius, dx:-2, dy:2, color: "red", nbHit:0, collisionCheck: false},
  //{x:canvasW/5, y:canvasH/5, radius:radius, dx:-2, dy:2, color: "yellow", nbHit:0, collisionCheck: false},
  //{x:canvasW/3, y:canvasH/3, radius:radius, dx:-2, dy:2, color: "blue", nbHit:0, collisionCheck: false}
];

// Dessin des cercles
function drawBall(){
  for (var i = 0; i < shapes.length; i++) {
    let shape = shapes[i];
    ctx.beginPath();

    ctx.strokeStyle = shape.color;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.font = "30px Arial";
    ctx.fillText(shape.nbHit, shape.x, shape.y);
    ctx.lineWidth="15";

    ctx.arc(shape.x, shape.y, shape.radius, 0, Math.PI*2);
    ctx.stroke();
    // ctx.fillStyle = shape.color;
    // ctx.fill();
    ctx.closePath();
  }
}

//Dessin du canvas
function draw(){
  ctx.clearRect(0,0,canvasW,canvasH);
  drawBall();
  let alertCollision = checkCircle(collisionFlag);

  //Gestion des collisions
    for (var i = 0; i < shapes.length; i++) {
      let shape = shapes[i];
      //Collision murs Haut/Bas
      if(shape.y + shape.dy < shape.radius || shape.y + shape.dy > canvasH-shape.radius) {
          shape.dy = -shape.dy;
          shape.nbHit++;
      }
      //Collision murs Gauche/Droite
      if(shape.x + shape.dx < shape.radius || shape.x + shape.dx > canvasW-shape.radius) {
          shape.dx = -shape.dx;
          shape.nbHit++;
      }
      //Collision entre les cercles
      if(alertCollision && shape.collisionCheck == true) {
          shape.collisionCheck = false;
          //console.log(shape.collisionCheck);
          shape.dx = -shape.dx;
          shape.dy = -shape.dy;
          shape.nbHit++;
      }
      //MaJ des coordonnées
      shape.x += shape.dx;
      shape.y += shape.dy;
    }
}

//Vérification de collision entre les cercles
function checkCircle(collision) {
  //On parcours tous les cercles
  shapes.forEach((circle, i) => {

    //Puis on compare avec les autres cercles
    for (var j = 0; j < shapes.length; j++) {

      //Si les deux cercles comparés sont différents
      if (i != j) {
        //Alors on regarde la distance entre les deux
        let checkDistance = getDistance(circle, shapes[j]);

        //Si la distance est inférieur à 2 fois le rayon alors il y a collision
        if (checkDistance < radius*2) {
          collision = true;
          circle.collisionCheck = true;
          //console.log(circle.collisionCheck);
          //S'il y a collision, la fonction retourne Vrai
          return collision;
        }
      }
    }
  });
  //Sinon elle retourne Faux
  return collision;
}

//Récupération de la distance entre deux cercles
function getDistance(circle1, circle2) {
  //On calcule la distance entre deux cercles avec "a^2 + b^2 = c^2" (ici la fonction Math.hypot)
  let distance = Math.hypot((circle1.x - circle2.x), (circle1.y - circle2.y));
  //On retourne la valeur correspondant à la distance entre les deux centres des cercles
  return distance;
}

//Trouver les coordonnées de la souris
function getMousePosition(){
  let canvasPosition = canvas.getBoundingClientRect();
  xMouse = event.clientX - canvasPosition.left;
  yMouse = event.clientY - canvasPosition.top;
}

//Ajouter un cerlce à l'emplacement du click
function addCircle(){
  getMousePosition();
  shapes.push({x:xMouse, y:yMouse, radius:radius, dx:-2, dy:2, color: getRandomColor(), nbHit:0, collisionCheck: false});
}

canvas.addEventListener("mousedown", addCircle);

//couleur aléatoire
function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

//On redessine le Canvas toutes les 10 millisecondes
setInterval(draw, 10);
